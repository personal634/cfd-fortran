# CFD Fortran

Simple CFD simulator by means of stable
algorithms. Includes:

* Source code (100% Fortran) with the required
  functions and subroutines in order to run a
  complete example. This example consists on a
  (buggy - blame the boundary conditions -)
  square grid with two inlets sharing a
  common corner and two outlets sharing the
  opposite corner. A little change on the
  velocity boundary conditions at the bottom
  of the main source code would yield different
  (and prettier) results.
* Some gif/mp4 examples and weird simulation
  results.
* Scripts for:
    * ``CMakeLists.txt``: CMake project builder
    (requires cmake).
    * ``build_doc.sh``: Building the
    documentation (requires building a Python
    virtualenv ``.venv`` directory at the root
    and installing the dependencies at
    ``requirements.txt``).
    * ``build_animation.sh``: Building the dye
    animations after running the program
    (requires ffmpeg).
    * ``fifo2D.py``: Building the velocity
    field's X-component animation.

## Requirements

* Minimal requirements:
  * Unix/Linux (Windows support would require
    changing the POSIX paths at ``main.f90``).
  * CMake
  * Any Fortran compiler (ifort/gfortran/g95/gcc/..).
* Dye animations requirements:
  * ffmpeg.
* Documentation and animation building
  requirements:
  * Python3 and virtualenv.

## Building and running the program.

The following sections describe the required
steps in order to build, execute the program,
generate the animations and build the
documentation.

These steps are divided in sections so that
the user gets to choose how far it wants to
reach.

### Build and execute main program.

The Fortran CMake project can be built
and executed by clicking the correct
buttons on your favorite IDE (recommended)
or by typing the following commands on the
terminal. In case of using the first option,
please do note the step number 2, the ``img``
directory must be manually created.

1. Build the CMake project, either a button
   on the IDE or:
   ```bash
   $ cmake --build ./cmake-build-release-gnu --target cfd_fortran -- -j 3
   ```
2. Create the ``img`` directory where the
   simulation's images will be stored:
   ```bash
   $ mkdir ./cmake-build-release-gnu/img
   ```
3. Run the executable (IDE or terminal):
   ```bash
   $ ./cmake-build-release-gnu/cfd_fortran
   ```
   
Now, the dye simulation images are stored at
``./cmake-build-release-gnu/img`` directory
and the data for computing the velocity
animation at ``/tmp/myinpt`` and ``/tmp/myfifo``.

### Build the dye animation.

Requires the ``ffmpeg`` program. Just run
the following script:

```bash
$ bash ./build_animation.sh
```

### Create the virtualenv.

Requires Python and virtualenv.

This step must be executed ONLY ONCE in
order to build the velocity animation and
the documentation.

```bash
$ python3 -m venv .venv
$ source .venv/bin/activate
$ pip install -r requirements.txt
$ deactivate
```

### Build the animations.

After creating the Python virtualenv and
building and running the program (see
above), build the velocity animation by
executing the following script
(may take a while):

```bash
$ source .venv/bin/activate
$ python ./fifo2D.py
$ deactivate
```

## Bibliography

* Algorithms:

  [Nvidia GPU Gems, Chapter 38. Fast Fluid Dynamics Simulation on the GPU](http://developer.download.nvidia.com/books/HTML/gpugems/gpugems_ch38.html)