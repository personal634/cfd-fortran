#!/bin/bash

CFD_FORTRAN_DOCS_HTML=1

# Build HTML docs (fast).
if [[ -z "${CFD_FORTRAN_DOCS_HTML}" ]]; then
  echo ;
else
  source .venv/bin/activate;
  cd docs || exit;
  make html;
  cd ..;
  deactivate;
  echo "HTML docs built!";
fi

# Build LaTeX-PDF docs (slow and ugly).
if [[ -z "${CFD_FORTRAN_DOCS_LATEX}" ]]; then
  echo ;
else
  source .venv/bin/activate;
  cd docs || exit;
  make latex;
  cd _build/latex || exit;
  make;
  cd ..;
  deactivate;
  echo "LaTeX docs built!";
fi
