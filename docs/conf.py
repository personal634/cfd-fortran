# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'cfd_fortran'
copyright = '2020, Imanol Sardón Delgado'
author = 'Imanol Sardón Delgado'

# The full version, including alpha/beta/rc tags
release = '0.1'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    #"sphinx.ext.autodoc",
    #"sphinx.ext.intersphinx",
    #"sphinx.ext.todo",
    #"sphinx.ext.mathjax",
    #"sphinx.ext.ifconfig",
    #"sphinx.ext.viewcode",
    #"sphinx.ext.napoleon",
    # "m2r",
    "sphinxfortran.fortran_autodoc",
    "sphinxfortran.fortran_domain"
]

fortran_src = []
for rel_path, ds, fs in os.walk(os.path.join('..', 'src', 'foam')):
    for f in fs:
        if f.endswith('f90') or f.endswith('f95'):
            fortran_src.append(os.path.abspath(os.path.join(rel_path, f)))
            pass
        pass
    pass

# fortran_src = fortran_src[0:1]
print('Fortran Source files:\n[\n\t', '\n\t'.join(fortran_src), '\n]')


# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']