#!/bin/bash

DEBUG_DIR="./cmake-build-debug/img"
RELEASE_DIR="./cmake-build-release/img"
RELEASE_GNU_DIR="./cmake-build-release-gnu/img"

if [ -d "$DEBUG_DIR" ]; then
  ffmpeg -r 25 -t 10 -i "$DEBUG_DIR"/img%04d.ppm "animation_debug.mp4" 2>/dev/null || printf "Empty dir %s.\n" $DEBUG_DIR;
else
  echo "Warning: no Debug configuration found.";
fi

if [ -d "$RELEASE_DIR" ]; then
  ffmpeg -r 25 -t 10 -i "$RELEASE_DIR"/img%04d.ppm "animation_release.mp4" 2>/dev/null || printf "Empty dir %s.\n" $RELEASE_DIR;
else
  echo "Warning: no Release configuration found.";
fi

if [ -d "$RELEASE_GNU_DIR" ]; then
  ffmpeg -r 25 -t 10 -i "$RELEASE_GNU_DIR"/img%04d.ppm "animation_release_gnu.mp4" 2>/dev/null || printf "Empty dir %s.\n" $RELEASE_GNU_DIR;
else
  echo "Warning: no Release-GNU configuration found.";
fi