module checks
    ! Subroutines that verify that certain conditions are met.
    implicit none
    private
    public :: check_size

    interface check_size
        procedure check_size_1d, check_size_2d
    end interface check_size
    contains

    subroutine check_size_1d(x1, x2, msg, exit_no)
        ! Check that length(x1) == length(x2).
        real, intent(in) :: x1(:)  ! First vector.
        real, intent(in) :: x2(:)  ! Second vector.
        character(len=*), intent(in), optional :: msg  ! Print message in case of size mismatch.
        integer, intent(in), optional :: exit_no   ! Exit error number in case of size mismatch.

        if(size(x1,1) /= size(x2,1)) then
            if(present(msg)) write(*,*) msg
            if(present(exit_no)) call exit(exit_no)
            call exit(1)
        end if
    end subroutine check_size_1d

    subroutine check_size_2d(x1, x2, msg, exit_no)
        ! Check that length(x1) == length(x2) for all available dimensions.
        real, intent(in) :: x1(:,:)  ! First matrix.
        real, intent(in) :: x2(:,:)  ! Second matrix.
        character(len=*), intent(in), optional :: msg  ! Print message in case of size mismatch.
        integer, intent(in), optional :: exit_no   ! Exit error number in case of size mismatch.

        if(size(x1,1) /= size(x2,1) .or. size(x1,2) /= size(x2,2)) then
            if(present(msg)) write(*,*) msg
            if(present(exit_no)) call exit(exit_no)
            call exit(1)
        end if
    end subroutine check_size_2d
end module checks