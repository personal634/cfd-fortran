module interfaces
    ! Common function/subroutine interfaces used on the library.
    implicit none

    public :: if_bc_u, if_bc_p, if_add_forces

    interface
        subroutine if_bc_u(ux, uy)
            ! Velocity boundary condition subroutine interface.
            real, intent(inout) :: ux(:,:)  ! Velocity's X-component as 2D-field.
            real, intent(inout) :: uy(:,:)  ! Velocity's Y-component as 2D-field.
        end subroutine if_bc_u

        subroutine if_bc_p(p)
            ! Pressure boundary condition subroutine interface.
            real, intent(inout) :: p(:,:)  ! Pressure 2D-field.
        end subroutine if_bc_p

        subroutine if_add_forces(ux, uy)
            ! Extern force subroutine interface.
            real, intent(inout) :: ux(:,:)  ! Velocity's X-component as 2D-field.
            real, intent(inout) :: uy(:,:)  ! Velocity's Y-component as 2D-field.
        end subroutine if_add_forces
    end interface
end module interfaces