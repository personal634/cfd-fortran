module boundary_conditions
    ! Includes several functions for setting velocity, pressure (or any other
    ! quantity) boundary conditions on a rectangular grid.
    use zero_gradient
    use fixed_value
    implicit none

    public :: free_stream_we, free_stream_sn, wall_slip

    contains

        subroutine free_stream_we(q, qw, include_corners, invert)
            ! Free stream current: the West wall acts as an inlet with a
            ! ``qw`` quantity and East wall as an outlet with uniform
            ! quantity (zero gradient).
            real, intent(inout) :: q(:,:)  ! Free stream quantity as 2D field where the BC is set.
            real, intent(in) :: qw  ! Inlet quantity.
            logical, intent(in), optional :: include_corners  ! (default=false) include corners as part of the wall (infinite wall).
            logical, intent(in), optional :: invert  ! (default=false) place inlet at East and outlet at West.

            integer :: m, n, k
            logical :: present_include_corners, present_invert

            m = size( q , 1 )
            n = size( q , 2 )
            present_include_corners = (present(include_corners)) .and. include_corners
            present_invert = (present(invert)) .and. invert

            if(present_invert) then
                ! Free stream E->W
                call bc_fixed_value_ew(q, [(qw, k=1, n-2)])
                call bc_zero_gradient_ww(q)
            else
                ! Free stream W->E
                call bc_fixed_value_ww(q, [(qw, k=1, n-2)])
                call bc_zero_gradient_ew(q)
            end if

            if(present_include_corners) then
                if(present_invert) then
                    q(m, 1) = 2e0 * qw - q(m-1, 1)
                    q(m, n) = 2e0 * qw - q(m-1, n)
                    q(1, 1) = q(2, 1)
                    q(1, n) = q(2, n)
                else
                    q(1, 1) = 2e0 * qw - q(2, 1)
                    q(1, n) = 2e0 * qw - q(2, n)
                    q(m, 1) = q(m-1, 1)
                    q(m, n) = q(m-1, n)
                end if
            end if
        end subroutine free_stream_we

        subroutine free_stream_sn(q, qw, include_corners, invert)
            ! Free stream current: simmilar to *free_stream_we*, the South
            ! wall acts as an inlet wall with a ``qw`` quantity and North
            ! wall as an outlet with uniform quantity (zero gradient).
            real, intent(inout) :: q(:,:)  ! Free stream quantity as 2D field where the BC is set.
            real, intent(in) :: qw  ! Inlet quantity.
            logical, intent(in), optional :: include_corners  ! (default=false) include corners as part of the wall (infinite wall).
            logical, intent(in), optional :: invert  ! (default=false) place inlet at East and outlet at West.

            integer :: m, n
            real, allocatable :: q_t(:,:)
            logical :: present_include_corners, present_invert

            m = size( q , 1 )
            n = size( q , 2 )
            allocate(q_t(1:n,1:m))
            q_t = transpose(q)
            present_include_corners = (present(include_corners)) .and. include_corners
            present_invert = (present(invert)) .and. invert

            call free_stream_we(q_t, qw, present_include_corners, .not. present_invert)

            q = transpose(q_t)
            deallocate(q_t)

        end subroutine free_stream_sn

        subroutine wall_slip(ux, uy, id, include_corners)
            ! Wall (slip): wall BC condition where tangent velocity component
            ! is kept and normal component is null.
            real, intent(inout), target :: ux(:,:)  ! Velocity (X-component) 2D-field.
            real, intent(inout), target :: uy(:,:)  ! Velocity (Y-component) 2D-field.
            integer, intent(in) :: id  ! Wall ID: 0->South, 1->East, 2->North, 3-> West.
            logical, intent(in), optional :: include_corners  ! (default=false) include corners as part of the wall (infinite wall).

            integer :: m, n, bound_size_x, bound_size_y, lbx, ubx, lby, uby
            real, pointer :: boundary_int_x(:), boundary_ext_x(:)
            real, pointer :: boundary_int_y(:), boundary_ext_y(:)

            logical :: present_include_corners

            m = size( ux , 1 )
            n = size( ux , 2 )
            present_include_corners = (present(include_corners)) .and. include_corners
            if(present_include_corners) then
                bound_size_x = m
                bound_size_y = n
                lbx = 1
                ubx = m
                lby = 1
                uby = n
            else
                bound_size_x = m - 2
                bound_size_y = n - 2
                lbx = 2
                ubx = m - 1
                lby = 2
                uby = n - 1
            end if

            if(size(uy,1) /= m .or. size(uy,2) /= n) then
                write(*, *) "Error! wall_slip: ux and uy size mismatch."
                call exit(1)
            end if

            if(id < 0 .or. id >= 4) then
                write(*, *) "Error! wall_slip: id not in [0, 3]"
                call exit(1)
            else if(id == 0) then
                ! South wall.
                boundary_ext_x(1:bound_size_x) => ux(lbx:ubx,1)
                boundary_int_x(1:bound_size_x) => ux(lbx:ubx,2)
                boundary_ext_y(1:bound_size_y) => uy(lbx:ubx,1)
                boundary_int_y(1:bound_size_y) => uy(lbx:ubx,2)
            else if(id == 1) then
                ! East wall.
                boundary_ext_x(1:bound_size_x) => ux(m  ,lby:uby)
                boundary_int_x(1:bound_size_x) => ux(m-1,lby:uby)
                boundary_ext_y(1:bound_size_y) => uy(m  ,lby:uby)
                boundary_int_y(1:bound_size_y) => uy(m-1,lby:uby)
            else if(id == 2) then
                ! North wall.
                boundary_ext_x(1:bound_size_x) => ux(lbx:ubx,n  )
                boundary_int_x(1:bound_size_x) => ux(lbx:ubx,n-1)
                boundary_ext_y(1:bound_size_y) => uy(lbx:ubx,n  )
                boundary_int_y(1:bound_size_y) => uy(lbx:ubx,n-1)
            else if(id == 3) then
                ! West wall.
                boundary_ext_x(1:bound_size_x) => ux(1,lby:uby)
                boundary_int_x(1:bound_size_x) => ux(2,lby:uby)
                boundary_ext_y(1:bound_size_y) => uy(1,lby:uby)
                boundary_int_y(1:bound_size_y) => uy(2,lby:uby)
            end if

            if(mod(id, 2) == 0) then
                ! South or North wall -> Uy=0, grad(Ux)=0.
                boundary_ext_y = - boundary_int_y
                boundary_ext_x =   boundary_int_x
            else
                ! West or East wall -> Ux=0, grad(Uy)=0.
                boundary_ext_y =   boundary_int_y
                boundary_ext_x = - boundary_int_x
            end if

        end subroutine wall_slip
end module boundary_conditions