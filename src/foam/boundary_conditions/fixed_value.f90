module fixed_value
    implicit none

    public

    contains


        subroutine bc_fixed_value(phi, b)
            ! bc_fixed_value(phi: R[MxN], b: R[2*(M-1)+2*(N-1)])
            !
            ! Sets the boundary conditions so that the interpolation of a
            ! boundary cell and an inner cell equals the set value.
            !
            ! The boundary values circle around the boundary wall on an
            ! anti-clockwise direction where the first value equals to the
            ! boundary condition at the bottom-left corner.
            real, intent(inout) :: phi(:, :)  ! 2D field where BC are set.
            real, intent(in) :: b(:)  ! Fixed values to be set.

            integer :: m, n

            m = size( phi, 1 )
            n = size( phi, 2 )

            if (size(b, 1) /= 2*(m-1) + 2*(n-1)) then
                write(*, *) "E: Fixed value boundary condition."
                write(*, *) "   Boundary array b size mismatch."
                return
            end if

            !t1 = (m-1)+(n-1)
            !t2 = (m-1)
            !do k = 1, 2*(m-1)+2*(n-1)
            !    s = (k-1) / t1
            !    g = mod(k-1,t1)/t2
            !    e = mod(s+g, 2)
            !    ci = (1-g)*(1-2*s)
            !    cj = g * (1-2*s)

            !    i = m ** e + ci * mod(k-1, t1)
            !    j = m ** s + cj * mod((k-1) - (m-1), t1)
            !end do

            ! Set boundaries for each edge/wall.
            call bc_fixed_value_sw(phi, b(0*(m-1)+0*(n-1)+2:1*(m-1)+0*(n-1)))
            call bc_fixed_value_ew(phi, b(1*(m-1)+0*(n-1)+2:1*(m-1)+1*(n-1)))
            call bc_fixed_value_nw(phi, b(1*(m-1)+1*(n-1)+2:2*(m-1)+1*(n-1)))
            call bc_fixed_value_ww(phi, b(2*(m-1)+1*(n-1)+2:2*(m-1)+2*(n-1)))

            ! Set boundaries for each corner.
            call bc_fixed_value_bl(phi, b(0*(m-1)+0*(n-1)+1))
            call bc_fixed_value_br(phi, b(1*(m-1)+0*(n-1)+1))
            call bc_fixed_value_tr(phi, b(1*(m-1)+1*(n-1)+1))
            call bc_fixed_value_tl(phi, b(2*(m-1)+1*(n-1)+1))

        end subroutine bc_fixed_value


        subroutine bc_fixed_value_nw(phi, b)
            ! bc_fixed_value_nw(phi: R[MxN], b: R[M-2])
            !
            !     Sets the fixed value boundary condition at north wall (j=n).
            !     Boundary values goes from right to left (anti-clockwise).
            real, intent(inout) :: phi(:, :)  ! M-by-N 2D field.
            real, intent(in) :: b(:)  ! (M-2) vector containing the boundary fixed values.

            integer :: i, j, m, n

            m = size( phi, 1 )
            n = size( phi, 2 )

            if (size(b, 1) /= m-2) then
                write(*, *) "E: Fixed value boundary condition NW."
                write(*, *) "   Boundary array b size mismatch."
                return
            end if

            j = n

            ! Top Edge.
            do i = m - 1, 2, -1
                phi(i, j) = 2e0 * b(m - i) - phi(i, j - 1)
            end do
        end subroutine bc_fixed_value_nw


        subroutine bc_fixed_value_sw(phi, b)
            ! bc_fixed_value_sw(phi: R[MxN], b: R[M-2])
            !
            !     Sets the fixed value boundary condition at south wall (j=1).
            !     Boundary values goes from left to right (anti-clockwise).
            real, intent(inout) :: phi(:, :)  ! M-by-N 2D field.
            real, intent(in) :: b(:)  ! (M-2) vector containing the boundary fixed values.

            integer :: i, j, m, n

            m = size( phi, 1 )
            n = size( phi, 2 )

            if (size(b, 1) /= m-2) then
                write(*, *) "E: Fixed value boundary condition SW."
                write(*, *) "   Boundary array b size mismatch."
                return
            end if

            j = 1

            ! Bottom Edge.
            do i = 2, m - 1
                phi(i, j) = 2e0 * b(i - 1) - phi(i, j + 1)
            end do
        end subroutine bc_fixed_value_sw


        subroutine bc_fixed_value_ew(phi, b)
            ! bc_fixed_value_ew(phi: R[MxN], b: R[N-2])
            !
            !     Sets the fixed value boundary condition at east wall (i=M).
            !     Boundary values goes from bottom to top (anti-clockwise).
            real, intent(inout) :: phi(:, :)  ! M-by-N 2D field.
            real, intent(in) :: b(:)  ! (N-2) vector containing the boundary fixed values.

            integer :: i, j, m, n

            m = size( phi, 1 )
            n = size( phi, 2 )

            if (size(b, 1) /= n-2) then
                write(*, *) "E: Fixed value boundary condition EW."
                write(*, *) "   Boundary array b size mismatch."
                return
            end if

            i = m

            ! Right Edge.
            do j = 2, n - 1
                phi(i, j) = 2e0 * b(j - 1) - phi(i-1, j)
            end do
        end subroutine bc_fixed_value_ew


        subroutine bc_fixed_value_ww(phi, b)
            ! bc_fixed_value_ww(phi: R[MxN], b: R[N-2])
            !
            !     Sets the fixed value boundary condition at west wall (i=1).
            !     Boundary values goes from top to bottom (anti-clockwise).
            real, intent(inout) :: phi(:, :)  ! M-by-N 2D field.
            real, intent(in) :: b(:)  ! (N-2) vector containing the boundary fixed values.

            integer :: i, j, m, n

            m = size( phi, 1 )
            n = size( phi, 2 )

            if (size(b, 1) /= n-2) then
                write(*, *) "E: Fixed value boundary condition WW."
                write(*, *) "   Boundary array b size mismatch."
                return
            end if

            i = 1

            ! Left Edge.
            do j = n - 1, 2, - 1
                phi(i, j) = 2e0 * b(n - j) - phi(i + 1, j)
            end do
        end subroutine bc_fixed_value_ww


        subroutine bc_fixed_value_bl(phi, b)
            ! bc_fixed_value_bl(phi: R[MxN], b: R)
            !
            !     Sets the fixed value boundary condition ``b`` at bottom-left
            !     corner.
            real, intent(inout) :: phi(:, :)  ! M-by-N 2D field.
            real, intent(in) :: b  ! Fixed value condition.

            phi(1, 1) = 4e0 * b
            phi(1, 1) = phi(1, 1) - phi(2, 1)
            phi(1, 1) = phi(1, 1) - phi(1, 2)
            phi(1, 1) = phi(1, 1) - phi(2, 2)
        end subroutine bc_fixed_value_bl


        subroutine bc_fixed_value_br(phi, b)
            ! bc_fixed_value_br(phi: R[MxN], b: R)
            !
            !     Sets the fixed value boundary condition ``b`` at bottom-right
            !     corner.
            real, intent(inout) :: phi(:, :)  ! M-by-N 2D field.
            real, intent(in) :: b  ! Fixed value condition.

            integer :: m

            m = size( phi , 1 )

            phi(m, 1) = 4e0 * b
            phi(m, 1) = phi(m, 1) - phi(m-1, 1)
            phi(m, 1) = phi(m, 1) - phi(m  , 2)
            phi(m, 1) = phi(m, 1) - phi(m-1, 2)
        end subroutine bc_fixed_value_br


        subroutine bc_fixed_value_tr(phi, b)
            ! bc_fixed_value_tr(phi: R[MxN], b: R)
            !
            !     Sets the fixed value boundary condition ``b`` at top-right
            !     corner.
            real, intent(inout) :: phi(:, :)  ! M-by-N 2D field.
            real, intent(in) :: b  ! Fixed value condition.

            integer :: m, n

            m = size( phi , 1 )
            n = size( phi , 2 )

            phi(m, n) = 4e0 * b
            phi(m, n) = phi(m, n) - phi(m-1, n  )
            phi(m, n) = phi(m, n) - phi(m  , n-1)
            phi(m, n) = phi(m, n) - phi(m-1, n-1)
        end subroutine bc_fixed_value_tr


        subroutine bc_fixed_value_tl(phi, b)
            ! bc_fixed_value_tl(phi: R[MxN], b: R)
            !
            !     Sets the fixed value boundary condition ``b`` at top-left
            !     corner.
            real, intent(inout) :: phi(:, :)  ! M-by-N 2D field.
            real, intent(in) :: b  ! Fixed value condition.

            integer :: n

            n = size( phi , 2 )

            phi(1, n) = 4e0 * b
            phi(1, n) = phi(1, n) - phi(2, n  )
            phi(1, n) = phi(1, n) - phi(1, n-1)
            phi(1, n) = phi(1, n) - phi(2, n-1)
        end subroutine bc_fixed_value_tl
end module fixed_value