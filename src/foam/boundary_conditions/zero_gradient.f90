module zero_gradient
    implicit none

    public
    contains

        ! --------------------------------------------------------------------
        ! bc_zero_gradient(phi: R[MxN])
        !     Sets the boundary conditions so that the interpolation of a
        ! boundary cell and an inner cell equals the set value.
        !
        !   f_w - f_i
        !  ---------- = 0
        !      Dl
        subroutine bc_zero_gradient(phi)
            real, intent(inout) :: phi(:, :)

            integer :: m, n

            m = size( phi, 1 )
            n = size( phi, 2 )

            ! Set boundaries for each edge/wall.
            call bc_zero_gradient_sw(phi)
            call bc_zero_gradient_ew(phi)
            call bc_zero_gradient_nw(phi)
            call bc_zero_gradient_ww(phi)

            call bc_zero_gradient_bl(phi)
            call bc_zero_gradient_br(phi)
            call bc_zero_gradient_tl(phi)
            call bc_zero_gradient_tr(phi)

        end subroutine bc_zero_gradient

        ! --------------------------------------------------------------------
        ! bc_zero_gradient_nw(phi: R[MxN])
        !     Sets the zero gradient boundary condition at north wall (j=n).
        subroutine bc_zero_gradient_nw(phi)
            real, intent(inout) :: phi(:, :)

            integer :: i, j, m, n

            m = size( phi, 1 )
            n = size( phi, 2 )

            j = n

            ! Top Edge.
            do i = m - 1, 2, -1
                phi(i, j) = phi(i, j - 1)
            end do
        end subroutine bc_zero_gradient_nw

        ! --------------------------------------------------------------------
        ! bc_zero_gradient_sw(phi: R[MxN])
        !     Sets the zero gradient boundary condition at south wall (j=1).
        subroutine bc_zero_gradient_sw(phi)
            real, intent(inout) :: phi(:, :)

            integer :: i, j, m, n

            m = size( phi, 1 )
            n = size( phi, 2 )

            j = 1

            ! Bottom Edge.
            do i = 2, m - 1
                phi(i, j) = phi(i, j + 1)
            end do
        end subroutine bc_zero_gradient_sw

        ! --------------------------------------------------------------------
        ! bc_zero_gradient_ew(phi: R[MxN])
        !     Sets the zero gradient boundary condition at east wall (i=M).
        subroutine bc_zero_gradient_ew(phi)
            real, intent(inout) :: phi(:, :)

            integer :: i, j, m, n

            m = size( phi, 1 )
            n = size( phi, 2 )

            i = m

            ! Right Edge.
            do j = 2, n - 1
                phi(i, j) = phi(i-1, j)
            end do
        end subroutine bc_zero_gradient_ew

        ! --------------------------------------------------------------------
        ! bc_zero_gradient_ww(phi: R[MxN])
        !     Sets the zero gradient boundary condition at west wall (i=1).
        subroutine bc_zero_gradient_ww(phi)
            real, intent(inout) :: phi(:, :)

            integer :: i, j, m, n

            m = size( phi, 1 )
            n = size( phi, 2 )

            i = 1

            ! Left Edge.
            do j = n - 1, 2, - 1
                phi(i, j) = phi(i+1, j)
            end do
        end subroutine bc_zero_gradient_ww

        ! --------------------------------------------------------------------
        ! bc_zero_gradient_bl(phi: R[MxN])
        !     Sets the zero gradient boundary condition at bottom-left corner
        !     (i=1, j=1). Satisfies the following condition:
        !     _   ·-  __    -·
        !     n · |   \/ phi | (i=i*, j=j*) = 0
        !         ·-        -·
        !     With n = (-1, -1)/sqrt(2), i*=1, j*=1.
        !     The result is f_corner = f_diagonal.
        subroutine bc_zero_gradient_bl(phi)
            real, intent(inout) :: phi(:, :)

            phi(1, 1) = phi(2, 2)

        end subroutine bc_zero_gradient_bl

        ! --------------------------------------------------------------------
        ! bc_zero_gradient_br(phi: R[MxN])
        !     Sets the zero gradient boundary condition at bottom-right corner
        !     (i=M, j=1). Satisfies the following condition:
        !     _   ·-  __    -·
        !     n · |   \/ phi | (i=i*, j=j*) = 0
        !         ·-        -·
        !     With n = (1, -1)/sqrt(2), i*=M, j*=1.
        !     The result is f_corner = f_diagonal.
        subroutine bc_zero_gradient_br(phi)
            real, intent(inout) :: phi(:, :)

            integer :: m

            m = size( phi, 1 )

            phi(m, 1) = phi(m-1, 2)

        end subroutine bc_zero_gradient_br

        ! --------------------------------------------------------------------
        ! bc_zero_gradient_tl(phi: R[MxN])
        !     Sets the zero gradient boundary condition at top-left corner
        !     (i=1, j=N). Satisfies the following condition:
        !     _   ·-  __    -·
        !     n · |   \/ phi | (i=i*, j=j*) = 0
        !         ·-        -·
        !     With n = (-1, 1)/sqrt(2), i*=1, j*=N.
        !     The result is f_corner = f_diagonal.
        subroutine bc_zero_gradient_tl(phi)
            real, intent(inout) :: phi(:, :)

            integer :: n

            n = size( phi, 2 )

            phi(1, n) = phi(2, n-1)

        end subroutine bc_zero_gradient_tl

        ! --------------------------------------------------------------------
        ! bc_zero_gradient_tr(phi: R[MxN])
        !     Sets the zero gradient boundary condition at top-right corner
        !     (i=M, j=N). Satisfies the following condition:
        !     _   ·-  __    -·
        !     n · |   \/ phi | (i=i*, j=j*) = 0
        !         ·-        -·
        !     With n = (1, 1)/sqrt(2), i*=M, j*=N.
        !     The result is f_corner = f_diagonal.
        subroutine bc_zero_gradient_tr(phi)
            real, intent(inout) :: phi(:, :)

            integer :: m, n

            m = size( phi, 1 )
            n = size( phi, 2 )

            phi(m, n) = phi(m-1, n-1)

        end subroutine bc_zero_gradient_tr
end module zero_gradient