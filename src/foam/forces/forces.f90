module forces
    ! Several forces which may be applied to the fluid dynamics.
    use checks, only: check_size
    use custom_math, only: operator(.times.), times_r3
    implicit none

    private
    public :: add_force_centrifugal, add_force_rotation, add_force_centripetal
    contains

    subroutine add_force_centrifugal(ux, uy, dt, grid_sizes, rho, omega, x_c)
        ! Add centrifugal force to control volume:
        !
        !.. math::
        !
        !    \vec F_{centrifugal}(\vec x) = -m\Omega\vec k\times\Omega \vec k\times\left(\vec x - \vec x_c\right)
        real, intent(inout) :: ux(:,:)
        real, intent(inout) :: uy(:,:)
        real, intent(in) :: dt  ! Time step.
        real, intent(in) :: grid_sizes(2)  ! Problem's physical size.
        real, intent(in) :: rho  ! Fluid density.
        real, intent(in) :: omega  ! Angular velocity.
        real, intent(in) :: x_c(2)  ! Rotation central point as X-Y pair.

        integer :: i, j, m, n
        real :: cell_mass, force(2)

        m = size( ux , 1 )
        n = size( ux , 2 )

        call check_size(ux, uy, msg="Error! add_force_centrifugal: ux and uy size mismatch.")

        cell_mass = rho * product(grid_sizes) / float(m * n)

        do i = 1, m
            do j = 1, n
                force = force_centrifugal(cell_mass, omega, x_c, [(i-1e0)*grid_sizes(1)/(m-1e0), (j-1e0)*grid_sizes(2)/(n-1e0)])
                ux(i, j) = ux(i, j) + DT * force(1)
                uy(i, j) = uy(i, j) + DT * force(2)
            end do
        end do

    end subroutine add_force_centrifugal

    subroutine add_force_centripetal(ux, uy, dt, grid_sizes, rho, omega, x_c)
        ! Add centrifugal force to control volume:
        !
        !.. math::
        !
        !    \vec F_{centrifugal}(\vec x) = -m\Omega\vec k\times\Omega \vec k\times\left(\vec x - \vec x_c\right)
        real, intent(inout) :: ux(:,:)
        real, intent(inout) :: uy(:,:)
        real, intent(in) :: dt  ! Time step.
        real, intent(in) :: grid_sizes(2)  ! Problem's physical size.
        real, intent(in) :: rho  ! Fluid density.
        real, intent(in) :: omega  ! Angular velocity.
        real, intent(in) :: x_c(2)  ! Rotation central point as X-Y pair.

        integer :: i, j, m, n
        real :: cell_mass, force(2)

        m = size( ux , 1 )
        n = size( ux , 2 )

        call check_size(ux, uy, msg="Error! add_force_centrifugal: ux and uy size mismatch.")

        cell_mass = rho * product(grid_sizes) / float(m * n)

        do i = 1, m
            do j = 1, n
                force = -force_centrifugal(cell_mass, omega, x_c, [(i-1e0)*grid_sizes(1)/(m-1e0), (j-1e0)*grid_sizes(2)/(n-1e0)])
                ux(i, j) = ux(i, j) + DT * force(1)
                uy(i, j) = uy(i, j) + DT * force(2)
            end do
        end do

    end subroutine add_force_centripetal

    subroutine add_force_rotation(ux, uy, dt, grid_sizes, rho, omega, x_c)
        ! Add rottional force to control volume:
        !
        !.. math::
        !
        !    \vec F_{rot}(\vec x) = m\left|\Omega\right|\Omega\vec k\times\left(\vec x - \vec x_c\right)
        real, intent(inout) :: ux(:,:)
        real, intent(inout) :: uy(:,:)
        real, intent(in) :: dt  ! Time step.
        real, intent(in) :: grid_sizes(2)  ! Problem's physical size.
        real, intent(in) :: rho  ! Fluid density.
        real, intent(in) :: omega  ! Angular velocity.
        real, intent(in) :: x_c(2)  ! Rotation central point as X-Y pair.

        integer :: i, j, m, n
        real :: cell_mass, force(2)

        m = size( ux , 1 )
        n = size( ux , 2 )

        call check_size(ux, uy, msg="Error! add_force_centrifugal: ux and uy size mismatch.")

        cell_mass = rho * product(grid_sizes) / float(m * n)

        do i = 1, m
            do j = 1, n
                force = force_rotation(cell_mass, omega, x_c, [(i-1e0)*grid_sizes(1)/(m-1e0), (j-1e0)*grid_sizes(2)/(n-1e0)])
                ux(i, j) = ux(i, j) + DT * force(1)
                uy(i, j) = uy(i, j) + DT * force(2)
            end do
        end do

    end subroutine add_force_rotation

    function force_centrifugal(m, omega, x_c, x) result(f)
        ! Centrifugal force.
        real, intent(in) :: m  ! Cell mass (:math:`\rho\Delta x\Delta y`).
        real, intent(in) :: omega  ! Angular velocity.
        real, intent(in) :: x_c(2)  ! Rotation central point as X-Y pair.
        real, intent(in) :: x(2)  ! Cell position as X-Y pair.
        real :: f(2)  ! Centrifugal force.

        real :: r(3), w(3), f_r3(3)

        r = [x(1) - x_c(1), x(2) - x_c(2), 0e0]
        w = [0e0, 0e0, omega]

        f_r3 = - m * w .times. (w .times. r)
        f = [f_r3(1), f_r3(2)]
    end function force_centrifugal

    function force_rotation(m, omega, x_c, x) result(f)
        ! Rotational force around x_c.
        real, intent(in) :: m  ! Cell mass (:math:`\rho\Delta x\Delta y`).
        real, intent(in) :: omega  ! Angular velocity.
        real, intent(in) :: x_c(2)  ! Rotation central point as X-Y pair.
        real, intent(in) :: x(2)  ! Cell position as X-Y pair.
        real :: f(2)  ! Centrifugal force.

        real :: r(3), w(3), f_r3(3)

        r = [x(1) - x_c(1), x(2) - x_c(2), 0e0]
        w = [0e0, 0e0, omega]

        f_r3 = m * omega * times_r3(w, r)
        f = [f_r3(1), f_r3(2)]
    end function force_rotation
end module forces