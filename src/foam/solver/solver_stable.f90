module solver_stable
    ! Incompresible stable fluid dynamics solver.

    use advection, only: advection_stable_2d, bilerp
    use diffusion, only: diffusion_stable_2d => stable_2d, poisson_2d
    use checks, only: check_size
    use interfaces, only: if_bc_u, if_bc_p, if_add_forces
    implicit none

    private
    public :: solve

    contains

        subroutine solve(ux, uy, p, dt, grid_sizes, nu, velocity_bc, pressure_bc, add_forces, rms)
            ! Incompresible stable fluid dynamics solver.
            real, intent(inout) :: ux(:,:)  ! Velocity X-component as 2D-field.
            real, intent(inout) :: uy(:,:)  ! Velocity Y-component as 2D-field.
            real, intent(inout) :: p(:,:)  ! Pressure as 2D-field.
            real, intent(in) :: dt  ! Time step.
            real, intent(in) :: grid_sizes(2)  ! Problem's physical size.
            real, intent(in) :: nu  ! Kinematic viscosity.
            procedure(if_bc_u) :: velocity_bc  ! Subroutine that computes the velocity boundary condition.
            procedure(if_bc_p) :: pressure_bc  ! Subroutine that computes the pressure boundary condition.
            procedure(if_add_forces), optional :: add_forces  ! Subroutine that computes the outer forces.
            real, intent(in), optional :: rms  ! Minimal accepted RMS value for Poisson equation.

            integer :: m, n
            real, allocatable :: wx(:,:), wy(:,:)

            m = size( ux , 1 )
            n = size( uy , 2 )

            allocate(wx(1:m, 1:n), wy(1:m, 1:n))

            wx = advection_stable_2d(ux, ux, uy, dt, grid_sizes)
            wy = advection_stable_2d(uy, ux, uy, dt, grid_sizes)
            ux = wx
            uy = wy

            if(present(rms)) then
                call diffusion_stable_2d(ux, uy, dt, nu, grid_sizes, wx, wy, velocity_bc=velocity_bc, rms=rms)
            else
                call diffusion_stable_2d(ux, uy, dt, nu, grid_sizes, wx, wy, velocity_bc=velocity_bc)
            end if
            ux = wx
            uy = wy
            call velocity_bc(ux, uy)

            if(present(add_forces)) call add_forces(ux, uy)

            call velocity_bc(ux, uy)
            wx = ux
            wy = wy
            call project(ux, uy, p, wx, wy, grid_sizes, velocity_bc, pressure_bc)

        end subroutine solve


        subroutine project(ux, uy, p, wx, wy, grid_sizes, velocity_bc, pressure_bc)
            ! Project ensures that :math:`\vec\nabla\cdot\vec u=0` on all inner cells.
            !
            ! Calculates the pressure field by solving the Poisson equation
            ! and then substracts it from the velocity field.
            real, intent(inout) :: ux(:,:)  ! Velocity's X component 2D field.
            real, intent(inout) :: uy(:,:)  ! Velocity's Y component 2D field.
            real, intent(inout) :: p(:,:)  ! Pressure 2D field.
            real, intent(in) :: wx(:,:)  ! Non-zero divergence variable's X component 2D field.
            real, intent(in) :: wy(:,:)  ! Non-zero divergence variable's Y component 2D field.
            real, intent(in) :: grid_sizes(2)  ! Problem's physical size.
            procedure(if_bc_u) :: velocity_bc  ! Subroutine that computes the velocity boundary condition.
            procedure(if_bc_p) :: pressure_bc  ! Subroutine that computes the pressure boundary condition.

            integer :: i, j, m, n
            real :: dx, dy
            real, allocatable :: div_w(:, :)

            m = size( wx, 1 )
            n = size( wx, 2 )

            call check_size(wx, wy, msg="E: project: wx and wy size mismatch.")
            call check_size(wx, ux, msg="E: project: wx and ux size mismatch.")
            call check_size(wx, uy, msg="E: project: wx and uy size mismatch.")

            allocate(div_w(m, n))

            dx = grid_sizes(1) / m
            dy = grid_sizes(2) / n

            div_w = 0e0

            do i = 2, m - 1
                do j = 2, n - 1
                    div_w(i, j) = div_w(i, j) + (wx(i+1, j) - wx(i-1, j)) / 2e0 / dx
                    div_w(i, j) = div_w(i, j) + (wy(i, j+1) - wy(i, j-1)) / 2e0 / dy
                end do
            end do

            call poisson_2d(div_w, dx, p, phi_bc=pressure_bc)

            do i = 2, m - 1
                do j = 2, n - 1
                    ux(i, j) = wx(i, j) - (p(i+1, j) - p(i-1, j)) / 2e0 / dx
                    uy(i, j) = wy(i, j) - (p(i, j+1) - p(i, j-1)) / 2e0 / dy
                end do
            end do

            call velocity_bc(ux, uy)

        end subroutine project
end module