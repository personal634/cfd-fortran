module advection

! Advection functions for calculating the velocity advection (or any other quantity) by means of stable algorithms.

    use, intrinsic :: ieee_arithmetic, only: ieee_is_finite
    use checks, only: check_size
    implicit none

    private
    public :: advection_stable_2d, bilerp, stable_1d

    contains

    subroutine bounds(xs, x, lb, ub)
        ! Get lower and upper bound indexes so that:
        !
        ! :math:`Xs_{lb} \le x \le Xs_{ub} \land lb + 1 = ub`
        real, intent(in) :: xs(:)  ! Sorted vector (ascending order) in where the indexes are searched.
        real, intent(in) :: x  ! Value to be searched on ``xs`` vector (must be higher or equal to the vector's first element and lower or equal to the last.
        integer, intent(out) :: lb  ! Lower index bound.
        integer, intent(out) :: ub  ! Upper index bound (also, ub = lb + 1).

        integer :: m, i_half

        m = size(xs)

        if(any(isnan(xs)) .or. (.not. all(ieee_is_finite(xs)))) then
            write(*, *) "Error! bounds: NaNs or Infinitys on xs."
            call exit(1)
        end if

        if (x <= minval(xs)) then
            lb = 1
            ub = 2
        elseif (x >= maxval(xs)) then
            lb = m - 1
            ub = m
        else
            lb = 1
            ub = m
            do while(.not. (xs(lb) <= x .and. xs(lb+1) >= x))
                i_half = int((lb + ub) / 2)
                if (xs(i_half) <= x) then
                    lb = i_half
                else
                    ub = i_half
                end if
            end do
            ub = lb + 1
        end if

    end subroutine bounds

    function lerp(xs, fs, x) result(f)
        ! Linear interpolator that searches the x value in the sorted
        ! array xs, takes its xs lower and upper limit as well as its
        ! corresponding fs values and interpolates:
        !
        !.. math::
        !
        !    f=f_l+\frac{f_u-f_l}{x_u-x_l}\left(x-x_l\right)
        real, intent(in) :: xs(:)  ! X values.
        real, intent(in) :: x  ! X value to be searched in ``xs`` and interpolated linearly in ``fs``.
        real, intent(in) :: fs(:)  ! Y values.
        real :: f  ! Resulting interpolated value.

        integer :: i_l, i_u

        if (size(fs, 1) /= size(xs, 1)) then
            write(*, *) "Error! bilerp: xs and fs sizes mismatch."
            call exit(1)
        end if

        call bounds(xs, x, i_l, i_u)

        f = fs(i_l) + (fs(i_u) - fs(i_l)) / (xs(i_u) - xs(i_l)) * (x - xs(i_l))
    end function lerp

    function bilerp(xs, ys, fs, xy) result(f)
        ! Bi-Linear interpolator that searches the x and y values in the
        ! sorted arrays xs and ys, takes its lower and upper limits as
        ! well as its corresponding fs values and interpolates.
        !
        !.. math::
        !
        !    x_l \le x \le x_u \ /\  x_l, x_u \in \vec{xs}
        !
        !    y_l \le y \le y_u \ /\  y_l, y_u \in \vec{ys}
        !
        !    f_{ij} = f(x_i, y_j)
        !
        !    \hat x = \frac{x-x_l}{x_u-x_l}\  ,\ \hat y = \frac{y-y_l}{y_u-y_l}
        !
        !    \ f(x, y) = (1-\hat x)(1-\hat y)f_{ll} + (1-\hat x)\hat yf_{lu} + \hat x(1-\hat y)f_{ul} + \hat x\hat yf_{uu}
        real, intent(in) :: xs(:)  ! X values.
        real, intent(in) :: ys(:)  ! Y values.
        real, intent(in) :: xy(2)  ! X-Y pair of values to be searched in ``xs`` and ``ys`` and interpolated in ``fs``.
        real, intent(in) :: fs(:,:)  ! F values from which interpolate.
        real :: f  ! Interpolated value.

        integer :: m, n
        integer :: i_l, i_u, j_l, j_u
        real :: x, y, x_frac, y_frac
        real :: weights(4), points(4)

        m = size(xs)
        n = size(ys)

        if(any(isnan(fs)) .or. (.not. all(ieee_is_finite(fs)))) then
            write(*, *) "Error! bilerp: NaNs or Infinitys on fs."
            call exit(1)
        end if

        if (size(fs, 1) /= m .or. size(fs, 2) /= n) then
            write(*, *) "Error! bilerp: xs, ys and fs sizes mismatch."
            call exit(1)
        end if

        x = xy(1)
        y = xy(2)

        call bounds(xs, x, i_l, i_u)
        call bounds(ys, y, j_l, j_u)

        x_frac = (x - xs(i_l)) / (xs(i_u) - xs(i_l))
        y_frac = (y - ys(j_l)) / (ys(j_u) - ys(j_l))

        ! Bottom-left.
        weights(1) = (1e0 - x_frac) * (1e0 - y_frac)
        ! Bottom-right.
        weights(2) = x_frac * (1e0 - y_frac)
        ! Top-left.
        weights(3) = (1e0 - x_frac) * y_frac
        ! Top-right.
        weights(4) = x_frac * y_frac

        ! F-values, BL, BR, TL, TR
        points = [fs(i_l, j_l), fs(i_u, j_l), fs(i_l, j_u), fs(i_u, j_u)]

        f = sum(points * weights) / sum(weights)
    end function bilerp

    function advection_stable_2d(q, u_x, u_y, dt, grid_sizes) result(q_next)
        ! Solves the advenction problem:
        !
        !.. math::
        !    \frac{\text{d}\vec q}{\text{d}t}=-\vec u\cdot\nabla \vec q
        !
        ! It solves it by stable means in which a particle's ``q`` quantity at the
        ! :math:`\vec x_i` grid position is the same as it was at its previous
        ! position :math:`\vec x_j`.
        !
        !.. math::
        !    :nowrap:
        !
        !    \begin{eqnarray}
        !        \vec x_j &=& \vec x_i-\Delta t\cdot\vec u_i \\ q^{\left(n+1\right.} &=& \text{bilerp}\left(\vec q^{\left(n\right.}, \vec x, \vec x_j\right)
        !    \end{eqnarray}
        !
        real, intent(in) :: q(:,:)  ! :math:`M`-by-:math:`N` advected quantity 2D-field.
        real, intent(in) :: u_x(:,:)  ! :math:`M_2`-by-:math:`N_2` X-axis velocity 2D-field.
        real, intent(in) :: u_y(:,:)  ! :math:`M_2`-by-:math:`N_2` Y-axis velocity 2D-field.
        real, intent(in) :: dt  ! Time step.
        real, intent(in) :: grid_sizes(2)  ! Grid physical size as X-Y pair of values.
        real, allocatable :: q_next(:,:)  ! :math:`M`-by-:math:`N` Advected quantity on next time step.

        integer :: i, j, ii, jj, n, m, n2, m2, i_u, j_u
        real :: x, y, x_prev, y_prev, lx, ly

        m = size(q, 1)
        n = size(q, 2)

        m2 = size(u_x, 1)
        n2 = size(u_x, 2)

        call check_size(u_x, u_y, msg="E: advection_stable_2d: u_x and u_y size mismatch.")

        allocate(q_next(m, n))

        lx = grid_sizes(1)
        ly = grid_sizes(2)

        do i = 1, m
            do j = 1, n
                x = lx * (i - 1e0) / (m - 1e0)
                y = ly * (j - 1e0) / (n - 1e0)

                i_u = int(m2 * x / lx)
                j_u = int(n2 * y / ly)

                x_prev = x - u_x(i, j) * dt
                y_prev = y - u_y(i, j) * dt

                q_next(i, j) = bilerp([(ii*lx/(m-1e0), ii=0, m-1)], [(jj*ly/(n-1e0), jj=0, n-1)], q, [x_prev, y_prev])
            end do
        end do
    end function advection_stable_2d

    function stable_1d(q, u, dt, l) result(q_next)
        ! Solves the advenction problem:
        !
        !.. math::
        !
        !    \frac{\text{d}\vec q}{\text{d}t}=-\vec u\cdot\nabla \vec q
        !
        ! It solves it by stable means in which a particle's ``q`` quantity at the
        ! :math:`\vec x_i` grid position is the same as it was at its previous
        ! position :math:`\vec x_j`.
        !
        !.. math::
        !    :nowrap:
        !
        !    \begin{eqnarray}
        !        \vec x_j &=& \vec x_i-\Delta t\cdot\vec u_i \\ q^{\left(n+1\right.} &=& \text{bilerp}\left(\vec q^{\left(n\right.}, \vec x, \vec x_j\right)
        !    \end{eqnarray}
        !
        real, intent(in) :: q(:)  ! Advected quantity.
        real, intent(in) :: u(:)  ! Velocity.
        real, intent(in) :: dt  ! Time step.
        real, intent(in) :: l  ! Problem's physical length.
        real, allocatable, target :: q_next(:)  ! Advected quantity on next time step.

        integer :: i, ii, m, m2, i_u
        real :: x, x_prev

        m = size(q, 1)
        m2 = size(u, 1)

        allocate(q_next(m))

        do i = 1, m
            ! q_advected(X_now, T_next) = q(X_prev, T_now)
            ! X_prev = X_now - U_now * dT
            ! X_now = (j-1) * Dx
            x = (i - 1e0) * l / (m - 1e0)
            i_u = int(x * m2 / l)
            x_prev = x - u(i_u) * dt

            q_next(i) = lerp([(ii*l/(m-1e0), ii=0, m-1)], q, x_prev)
        end do
    end function stable_1d
end module advection