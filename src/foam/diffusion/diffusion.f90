module diffusion
! Diffusion functions for calculating the velocity diffusion (or any other quantity) by means of stable algorithms.
    use checks, only: check_size
    use interfaces, only: if_bc_u, if_bc_p
    implicit none
    private
    public:: stable_2d, poisson_2d, stable_2d_q

    contains

        subroutine poisson_2d_generic(a, b, phi, rms, max_iter, phi_bc)
            ! Solve the Poisson ecuation by Jacobi iteration:
            !
            !.. math::
            !
            !    \nabla^2\phi=b
            !
            ! The iteration reaches all mesh elements and solves the
            ! finite difference problem for each (i,j) phi component
            ! with any discretization:
            !
            !.. math::
            !
            !    \sum_{ii=1}^{3}\sum_{jj=1}^{3}a_{ii,jj}\phi_{i+ii-2,j+jj-2}=b_{i,j}
            real, intent(in) :: a(3,3)  ! Nabla operator finite difference 3-points discretization.
            real, intent(in) :: b(:,:)  ! b value of the equation.
            real, intent(inout), target :: phi(:,:)  ! Solution to the Poisson equation.
            real, intent(in), optional :: rms  ! Minimal accepted RMS value.
            integer, intent(in), optional :: max_iter  ! Maximum number of iterations.
            procedure(if_bc_p), optional :: phi_bc  ! Subroutine that computes phi's boundary conditions.

            integer :: i, j, ii, jj, m, n, iter, present_max_iter
            real, allocatable :: phi0(:, :)
            real :: rms_ok

            m = size(b, 1)
            n = size(b, 2)

            if(size(phi, 1) /= m .or. size(phi, 2) /= n) then
                write(*, *) "Error! poisson_2d_generic: b and phi size mismatch."
                call exit(1)
            end if

            if(present(rms)) then
                rms_ok = abs(rms)
            else
                rms_ok = 1e-3
            end if

            if(present(max_iter)) then
                present_max_iter = max_iter
            else
                present_max_iter = 100
            end if

            allocate(phi0(m, n))
            phi0 = phi + 1e0 + rms_ok
            iter = 0

            do while (sqrt(sum((phi - phi0) ** 2)/float(n*m)) > rms_ok .and. iter < present_max_iter)
                phi0 = phi
                do i = 2, m - 1
                    do j = 2, n - 1
                        phi(i, j) = b(i, j)
                        do ii = 1, 3
                            do jj = 1, 3
                                if (ii /= 2 .or. jj /= 2) phi(i, j) = phi(i, j) - a(ii, jj) * phi(i+ii-2, j+jj-2)
                            end do
                        end do
                        phi(i, j) = phi(i, j) / a(2, 2)
                    end do
                end do
                if(present(phi_bc)) call phi_bc(phi)

                iter = iter + 1
            end do

            if(iter >= present_max_iter) write(*, *) "W: max number of iterations reached."
        end subroutine poisson_2d_generic


        subroutine poisson_2d(b, dx, phi, rms, max_iter, phi_bc)
            ! Solve the Poisson ecuation by Jacobi iteration:
            !
            !.. math::
            !
            !    \nabla^2\phi=b
            !
            ! The iteration reaches all mesh elements and solves the
            ! 2nd order centered finite difference problem for each (i,j) phi
            ! component:
            !
            !.. math::
            !
            !    \frac{\phi_{i-1,j}-2\phi_{i,j}+\phi_{i+1,j}}{\Delta x^2} + \frac{\phi_{i,j-1}-2\phi_{i,j}+\phi_{i,j+1}}{\Delta y^2} = b_{i,j}
            real, intent(in), target :: b(:,:)  ! b value of the equation.
            real, intent(in) :: dx  ! Cell size (same size on X and Y for all cells).
            real, intent(inout), target :: phi(:,:)  ! Solution to the Poisson equation.
            real, intent(in), optional :: rms  ! Minimal accepted RMS value.
            procedure(if_bc_p), optional :: phi_bc  ! Subroutine that computes phi's boundary conditions.
            integer, intent(in), optional :: max_iter  ! Maximum number of iterations.

            integer :: i, j, m, n, iter, present_max_iter
            ! integer :: iter
            real, allocatable :: phi0(:, :)
            real :: rms_ok

            m = size(b, 1)
            n = size(b, 2)

            if(present(rms)) then
                rms_ok = abs(rms)
            else
                rms_ok = 1e-3
            end if

            if(present(max_iter)) then
                present_max_iter = max_iter
            else
                present_max_iter = 100
            end if

            if(size(phi, 1) /= m .or. size(phi, 2) /= n) then
                write(*, *) "Error! poisson_2d: b and phi size mismatch."
                call exit(1)
            end if

            allocate(phi0(m, n))


            phi0 = phi + 1e0 + rms_ok

            iter = 0

            do while (sqrt(sum((phi - phi0) ** 2)/float(n*m)) > rms_ok .and. iter < present_max_iter)
                phi0 = phi
                do i = 2, m - 1
                    do j = 2, n - 1
                        phi(i, j) = phi(i - 1, j) + phi(i + 1, j) + phi(i, j - 1) + phi(i, j + 1)
                        phi(i, j) = phi(i, j) - dx ** 2 * b(i, j)
                        phi(i, j) = phi(i, j) / 4e0
                    end do
                end do
                ! iter = iter + 1
                if(present(phi_bc)) call phi_bc(phi)
                iter = iter + 1
            end do

            if(iter >= present_max_iter) write(*, *) "W: max number of iterations reached."
            ! write(*, "(A, I5)") "NO. OF ITERATIONS:", iter
        end subroutine poisson_2d


        subroutine stable_2d(u_x, u_y, dt, nu, grid_sizes, u_x_next, u_y_next, velocity_bc, rms)
            ! Solves the advenction problem:
            !
            !.. math::
            !
            !    \frac{\text{d}\vec u}{\text{d}t}=\nu\cdot\nabla^2\vec u
            real, intent(in) :: u_x(:,:)  ! :math:`M`-by-:math:`N` 2D velocity field (X component).
            real, intent(in) :: dt  ! Time step.
            real, intent(in) :: nu  ! Kinematic viscuosity.
            real, intent(in) :: grid_sizes(2)  ! Problem's physical X-Y size.
            real, intent(in) :: u_y(:,:)  ! :math:`M`-by-:math:`N` 2D velocity field (Y component).
            real, intent(inout) :: u_x_next(:,:)  ! The :math:`M`-by-:math:`N` 2D velocity field on next time step (X component).
            real, intent(inout) :: u_y_next(:,:)  ! The :math:`M`-by-:math:`N` 2D velocity field on next time step (Y component).
            procedure(if_bc_u) :: velocity_bc
            real, intent(in), optional :: rms  ! Minimal accepted RMS value.

            integer :: m, n
            real :: a_x(3, 3), a_y(3, 3), dx, dy, dn, rx, ry

            m = size(u_x, 1)
            n = size(u_x, 2)

            if(size(u_y, 1) /= m .or. size(u_y, 2) /= n) then
                write(*, *) "Error! stable_2d: u_x and u_y size mismatch."
                call exit(1)
            end if

            if(size(u_x_next, 1) /= m .or. size(u_x_next, 2) /= n) then
                write(*, *) "Error! stable_2d: u_x and u_x_next size mismatch."
                call exit(1)
            end if

            if(size(u_y_next, 1) /= m .or. size(u_y_next, 2) /= n) then
                write(*, *) "Error! stable_2d: u_x and u_y_next size mismatch."
                call exit(1)
            end if

            dx = grid_sizes(1) / m
            dy = grid_sizes(2) / n
            dn = sqrt(nu * dt)

            rx = (dn / dx) ** 2
            ry = (dn / dy) ** 2

            u_x_next = u_x
            u_y_next = u_y

            a_x(1, :) = [0e0, -ry, 0e0]
            a_x(2, :) = [-rx, 1e0 + 2e0 * (rx + ry), -rx]
            a_x(3, :) = [0e0, -ry, 0e0]

            a_y = a_x

            u_x_next = u_x
            u_y_next = u_y

            if(present(rms)) then
                call poisson_2d_generic(a_x, u_x, u_x_next, rms=rms, max_iter=500, phi_bc=bc_ux)
                call poisson_2d_generic(a_y, u_y, u_y_next, rms=rms, max_iter=500, phi_bc=bc_uy)
            else
                call poisson_2d_generic(a_x, u_x, u_x_next, rms=1e-3, max_iter=500, phi_bc=bc_ux)
                call poisson_2d_generic(a_y, u_y, u_y_next, rms=1e-3, max_iter=500, phi_bc=bc_uy)
            end if


            contains

                subroutine bc_ux(ux)
                    real, intent(inout) :: ux(:,:)

                    real :: uy_temp(size(ux,1), size(ux,2))
                    call velocity_bc(ux, uy_temp)
                end subroutine bc_ux

                subroutine bc_uy(uy)
                    real, intent(inout) :: uy(:,:)

                    real :: ux_temp(size(uy,1), size(uy,2))
                    call velocity_bc(ux_temp, uy)
                end subroutine bc_uy

        end subroutine stable_2d


        subroutine stable_2d_q(q, dt, nu, grid_sizes, q_next, q_bc, rms)
            ! Solves the advenction problem:
            !
            !.. math::
            !
            !    \frac{\text{d}\vec q}{\text{d}t}=\nu\cdot\nabla^2\vec q
            real, intent(in) :: q(:,:)  ! :math:`M`-by-:math:`N` 2D quantity field.
            real, intent(in) :: dt  ! Time step.
            real, intent(in) :: nu  ! Kinematic viscuosity.
            real, intent(in) :: grid_sizes(2)  ! Problem's physical X-Y size.
            real, intent(inout) :: q_next(:,:)  ! The :math:`M`-by-:math:`N` 2D-field on next time step.
            procedure(if_bc_p) :: q_bc  ! The quantity ``q`` boundary condition subroutine.
            real, intent(in), optional :: rms  ! Minimal accepted RMS value.

            integer :: m, n
            real :: a(3, 3), dx, dy, dn, rx, ry

            m = size(q, 1)
            n = size(q, 2)

            call check_size(q, q_next, msg="Error! stable_2d_q: q and q_next size mismatch.")

            dx = grid_sizes(1) / m
            dy = grid_sizes(2) / n
            dn = sqrt(nu * dt)

            rx = (dn / dx) ** 2
            ry = (dn / dy) ** 2

            a(1, :) = [0e0, -ry, 0e0]
            a(2, :) = [-rx, 1e0 + 2e0 * (rx + ry), -rx]
            a(3, :) = [0e0, -ry, 0e0]

            q_next = q

            if(present(rms)) then
                call poisson_2d_generic(a, q, q_next, rms=rms, phi_bc=q_bc)
            else
                call poisson_2d_generic(a, q, q_next, rms=1e-3, phi_bc=q_bc)
            end if

        end subroutine stable_2d_q


        subroutine solve_poisson(ni, nj, b, alpha, beta, phi, rms)
            integer, intent(in) :: ni, nj
            real, intent(in), target :: b(1:ni*nj)
            real, intent(in) :: alpha, beta
            real, intent(inout), target :: phi( size(b) )
            real, intent(in), optional :: rms

            integer :: i, j
            real :: rms_ok, phi0( size(b) )
            real, pointer :: phi_grid(:, :), b_grid(:, :)

            if(.not. present(rms)) then
                rms_ok = 1e-3
            else
                rms_ok = rms
            end if
            if(rms_ok <=0e0) rms_ok = 1e-3

            ! phi0 = 0e0
            ! phi = 1e0
            phi0 = phi + 1e0 + rms_ok
            phi_grid(1:ni, 1:nj) => phi(1:ni*nj)
            b_grid(1:ni, 1:nj) => b(1:ni*nj)

            do while (sqrt(sum((phi - phi0) ** 2)/float(ni*nj)) > rms)
                phi0 = phi
                do i = 2, ni - 1
                    do j = 2, nj - 1
                        phi_grid(i, j) = phi_grid(i - 1, j) + phi_grid(i + 1, j) + phi_grid(i, j - 1) + phi_grid(i, j + 1)
                        phi_grid(i, j) = phi_grid(i, j) + alpha * b_grid(i, j)
                        phi_grid(i, j) = phi_grid(i, j) / beta
                    end do
                end do
            end do
        end subroutine solve_poisson

end module diffusion