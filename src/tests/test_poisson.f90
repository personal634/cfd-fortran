program test_poisson
    !use foam_stable, only: poisson => solve_poisson
    implicit none

    integer, parameter :: NI = 100, NJ = 800, N_RHO_LIM = 40
    real, parameter :: DX = 1e0, RHO_BODY = 1e0, PI = 4e0 * atan(1e0)
    real, parameter :: BETA = 4e0, ALPHA = -DX ** 2

    integer :: i, j
    real :: gx(NJ - 4)
    real, target :: b(NI * NJ), phi(NI * NJ)
    real, pointer :: b_grid(:, :), phi_grid(:, :)

    call init_b(b)
    call init_phi(phi)

    call solve_poisson(NI, NJ, b, ALPHA, BETA, phi, ep=1e-3)

    phi_grid(1:NI, 1:NJ) => phi(1: NI * NJ)
    b_grid(1:NI, 1:NJ) => b(1: NI * NJ)

    write(*,*) "PHI: "
    do j = 1, NJ
        write(*, "(3F15.5)") phi_grid(1:3, j)
    end do
    write(*,*) " "


    open(unit=11, file="test.csv", status="replace", action="write", form="formatted")
    write(*,*) "GX: "
    do j = 3, NJ - 2
        gx(j - 2) = - (phi_grid(2, j + 1) - phi_grid(2, j - 1)) / DX / 2e0
        write(*, "(F15.5)") gx(j - 2)
        write(11, "(I3, 3(A1, F15.5))") j - 2, ",", b_grid(NI/2, j - 2), ",", phi_grid(NI/2, j - 2), ",", gx(j - 2)
    end do
    write(*,*) " "
    close(11)


    contains

    subroutine init_b(b)
        real, intent(out), target :: b(:)
        integer :: i, j
        real, pointer :: b_grid(:, :)
        character(len=50) :: fmt_str

        write(fmt_str, "(1A1, 1I4, 1A6)") "(", NJ, "F15.5)"

        b_grid(1:NI, 1:NJ) => b(1:NI * NJ)

        b_grid = 0e0
        do i = 1, NI
            do j = 1, NJ
                if ((i - NI / 2) ** 2 + (j - NJ / 2) ** 2 < N_RHO_LIM ** 2 / 4e0) b_grid(i, j) = 4e0 * PI * RHO_BODY
            end do
        end do

        write(*,*) "B: "
        do i = 1, NI
            write(*, fmt_str) b_grid(i, :)
        end do
        write(*,*) " "
    end subroutine init_b

    subroutine init_phi(phi)
        real, intent(inout), target :: phi(:)
        real, pointer :: phi_grid(:, :)

        phi_grid(1:NI, 1:NJ) => phi(1:NI * NJ)

        phi_grid = 0e0
    end subroutine init_phi

    subroutine solve_poisson(ni, nj, b, alpha, beta, phi, ep)
        integer, intent(in) :: ni, nj
        real, intent(in), target :: b(1:ni*nj)
        real, intent(in) :: alpha, beta
        real, intent(out), target :: phi( size(b) )
        real, optional :: ep

        integer :: i, j, k
        real :: phi0( size(b) )
        real, pointer :: phi_grid(:, :), b_grid(:, :)

        if (.not. present(ep)) ep = 1e-3

        phi0 = 0e0
        phi = 1e0
        phi_grid(1:ni, 1:nj) => phi(1:ni*nj)
        b_grid(1:ni, 1:nj) => b(1:ni*nj)

        k = 0
        write(*, "(I5,A2,F15.7)") k, ": ", sqrt(sum((phi - phi0) ** 2))
        do while (sqrt(sum((phi - phi0) ** 2)) > ep)
            phi0 = phi
            do i = 2, ni - 1
                do j = 2, nj - 1
                    phi_grid(i, j) = phi_grid(i - 1, j) + phi_grid(i + 1, j) + phi_grid(i, j - 1) + phi_grid(i, j + 1)
                    phi_grid(i, j) = phi_grid(i, j) + alpha * b_grid(i, j)
                    phi_grid(i, j) = phi_grid(i, j) / beta
                end do
            end do

            k = k + 1
            write(*, "(I5,A2,F15.7)") k, ": ", sqrt(sum((phi - phi0) ** 2))
        end do
    end subroutine solve_poisson

end program test_poisson