program test_advection
use advection

    call test()
    contains
    subroutine test()
        integer, parameter :: N = 20
        real, parameter :: L = 1e0
        real, parameter :: Q_LB = 0e0, Q_UB = 1e0
        real, parameter :: T0 = 0e0, TF = 2.58e0, DT = 1e-3
        character(len=*), parameter :: out_file = "test_advection.dat"

        integer :: i
        real :: xs(N), us(N), qs(N), t
        character(len=9) :: fmt

        xs = [((i - 1e0) / N * L, i = 1, N)]
        us = [(-1e0, i = 1, N)]
        qs = [(0e0, i = 1, N)]
        qs(1) = 2e0 * Q_LB - qs(2)
        qs(N) = 2e0 * Q_UB - qs(N-1)
        t = T0
        write(fmt, "(A1, I2, A5, A1)") "(", N, "E14.7", ")"

        open(unit=17, file=out_file, status="replace", form="formatted")
        do while(t < TF)
            write(17, fmt) qs
            qs = stable_1d(qs, us, DT, L)
            qs(1) = 2e0 * Q_LB - qs(2)
            qs(N) = 2e0 * Q_UB - qs(N-1)
            t = t + DT
        end do
        close(17)

    end subroutine test
end program test_advection