program cfd_fortran_2
    ! use foam
    ! use Linear_systems
    ! use Non_Linear_Systems
    ! use Jacobian_module
    use advection, only: advection_stable_2d, bilerp
    use diffusion, only: diffusion_stable_2d => stable_2d, poisson_2d, diffusion_stable_2d_q => stable_2d_q
    use zero_gradient
    use fixed_value
    use boundary_conditions
    use solver_stable, only: solve
    use forces, only: add_force_centrifugal, add_force_centripetal, add_force_rotation
    implicit none

    integer, parameter :: NH = 31, NV = 31, NM = NH * NV, N = 3 * NM, ITER_MAX = 1000
    integer, parameter :: IMG_W_PX = 128, IMG_H_PX = 128
    real, parameter :: LH = 1e0, LV = 1e0
    real, parameter :: DH = LH / NH, DV = LV / NV
    real, parameter :: MU = 1e-3, RHO = 1e3, DP = 1e0, V = 2e0, OMEGA = 1e2
    real, parameter :: DT = 1e-3, T_MAX = 5e-1, T_P = 1e-1
    real, parameter :: NU = 8e-5, U0 = DH/DT, PI = 4e0 * atan(1e0)

    character(len=*), parameter :: PARAM_PIPE = "/tmp/myinpt"
    character(len=*), parameter :: PARAM_DATA = "/tmp/myfifo"
    character(len=*), parameter :: PARAM_IMAGE_TIT = "img/img"
    character(len=*), parameter :: PARAM_IMAGE_EXT = "ppm"

    integer :: i, j, iter = 0, status
    real :: t = 0e0
    real, target :: w(1: NM * 2)
    real, target :: s(N) = 10e0, d(0:2, NM) = 0e0
    real, pointer :: u(:), p(:), d_grid(:, :), ux(:, :), uy(:, :), p_grid(:, :), wx(:, :), wy(:, :)
    character(len=50) :: data_fmt, data_fmt2, image_filename

    integer, parameter :: NT = floor(T_MAX / DT)

    ! Initialize vars & pointers.
    u(1:NM * 2) => s(1: NM * 2)
    ux(1:NH, 1:NV) => s(1:NM)
    uy(1:NH, 1:NV) => s(NM+1:NM*2)
    wx(1:NH, 1:NV) => w(1:NM)
    wy(1:NH, 1:NV) => w(NM+1:NM*2)
    p(1:NM) => s(NM * 2 + 1: N)
    p_grid(1:NH, 1:NV) => s(NM*2+1:NM*3)
    d_grid(1:NH, 1:NV) => d(0, 1:NM)
    d(2, 1:NM) = 0e0
    d = 0e0

    !do i = ceiling(NH / 4e0), floor(NH * 3e0 / 4e0)
    !    do j = ceiling(NV / 4e0), floor(NV * 3e0 / 4e0)
    !        d_grid(i, j) = 1e0
    !    end do
    !end do

    do i = 1, NV
        do j = 1, NH
            ux(i, j) = 1e0 * (1e0 - 2e0 / (NV-1e0)*(i-1e0))  ! 0e0 * (2e0 / (NH * 1e0) * ((j - NH) * 1e0 / 2e0))
            uy(i, j) = 1e0 * (1e0 - 2e0 / (NH-1e0)*(j-1e0))  ! 0e0 * (2e0 / (NV * 1e0) * ((i - NV) * 1e0 / 2e0))
            p_grid(i, j) = 0e0 !20/ 4e0 * (mod(i, 2) - 5e-1)
        end do
    end do

    write(data_fmt2, "(A, I1, A)") "(A8, I", floor(log10(NV * NH * 1e0)) + 1, ", A6)"
    write(data_fmt, data_fmt2) "(1E10.2,", NV * NH, "E10.2)"
    write(*, *) data_fmt
    write(*, *) U0

    open(unit=11, file=PARAM_PIPE, status='replace', access='stream', form='formatted', iostat=status)
    write(11, "(3I6)") NT, NH, NV
    close(11)

    open(unit=11, file=PARAM_DATA, status='replace', access='stream', form='formatted', iostat=status)

    !call print_results(s)
    do while (t < T_MAX)
        ! call compute_w(u, p, w)
        ! call bc_w(w)

        call print_image(iter, d)

        call solve(ux, uy, p_grid, DT, [LH, LV], NU, velocity_bc=bc_u, pressure_bc=bc_p, rms=1e-2)

        do i = 0, 2
            d_grid(1:NH, 1:NV) => d(i, 1:NM)
            d_grid = advection_stable_2d(d_grid, ux, uy, DT, [LH, LV])
        end do

        d_grid(1:NH, 1:NV) => d(0, 1:NM)
        call diffusion_stable_2d_q(d_grid, DT, 1e-1, [LH, LV], d_grid, bc_d_r, rms=1e-1)
        d_grid(1:NH, 1:NV) => d(1, 1:NM)
        call diffusion_stable_2d_q(d_grid, DT, 1e-1, [LH, LV], d_grid, bc_d_g, rms=1e-1)
        d_grid(1:NH, 1:NV) => d(2, 1:NM)
        call diffusion_stable_2d_q(d_grid, DT, 1e-1, [LH, LV], d_grid, bc_d_b, rms=1e-1)

        call bc_d(d)

        iter = iter + 1
        t = t + DT

        if (any(isnan(s))) then
            write(*, *) "Is NAN: T=", t
            call print_results(s)
            stop 1
        end if

        write(11, data_fmt, advance='no') t, u(1:NM)

    end do
    close(11)

    !call print_results(s)

    call print_results(s)
    write(*,*) "Hello, World!"

    contains

        subroutine print_image(iter, d)
            integer, intent(in) :: iter
            real, intent(inout), target :: d(0:,1:)

            integer :: i, j, k, i2, j2
            real, pointer :: r, g, b
            real :: x, y
            real, parameter :: xs_frac(1:NH) = [(float(i)/float(NH-1),i=0,NH-1)]
            real, parameter :: ys_frac(1:NV) = [(float(j)/float(NV-1),j=0,NV-1)]
            real, pointer :: grid(:,:)
            real, target :: colors(0:2)

            r => colors(0)
            g => colors(1)
            b => colors(2)

            write(image_filename, "(A,I4.4,A,A)") PARAM_IMAGE_TIT, iter, '.', PARAM_IMAGE_EXT
            open(unit=13, file=image_filename, status="replace", form="formatted")
            write(13, "(A2)") "P3"
            write(13, "(I4, A1, I4)") IMG_W_PX - 2, ' ', IMG_H_PX - 2
            write(13, "(A3)") "255"
            ! write(*,*) d
            do i = 2, IMG_W_PX - 1
                do j = 2, IMG_H_PX - 1
                    x = (i-1e0)/float(IMG_W_PX)
                    y = (j-1e0)/float(IMG_H_PX)

                    i2 = ceiling((NH-1) * x)
                    j2 = ceiling((NV-1) * y)

                    if(i2 == NH-1) i2 = NH-2
                    if(j2 == NV-1) j2 = NV-2
                    if(i2 == 1) i2 = 2
                    if(j2 == 1) j2 = 2

                    do k = 0, 2
                        grid(1:NH, 1:NV) => d(k,1:NH*NV)
                        !write(*, *) "X:", xs_frac(i2:i2+1), x
                        !write(*, *) "Y:", ys_frac(j2:j2+1), y
                        colors(k) = bilerp(xs_frac(i2:i2+1), ys_frac(j2:j2+1), grid(i2:i2+1, j2:j2+1), [x, y])


                    end do
                    where(abs(colors) > 1e0) colors = 1e0
                    !write(*, *) colors

                    write(13, "(3I4)") floor(255 * abs(colors))


                    !if (norm2(colors) < 1e-2) then
                    !    write(13, "(3I4)") floor(255 * abs(colors))
                    !else
                    !    write(13, "(3I4)") abs(floor(255 * colors / norm2(colors)))
                    !end if

                    !if(any(abs(colors) > 1e0)) then
                    !    d(:, k) = abs(colors / norm2(colors))
                    !end if

                end do
            end do
            close(13)
        end subroutine print_image

        subroutine bc_u(ux, uy)
            real, intent(inout) :: ux(:, :), uy(:, :)

            !integer :: n

            !n = size(uy,2)

            call bc_zero_gradient_nw(ux)
            call bc_zero_gradient_sw(ux)
            call bc_zero_gradient_ew(uy)
            call bc_zero_gradient_ww(uy)
            call free_stream_we(ux, U0, .true.)
            call free_stream_sn(uy, U0, .true.)

            !call wall_slip(ux, uy, 0, .false.)
            !call wall_slip(ux, uy, 2, .false.)
            !call wall_slip(ux, uy, 1, .true.)
            !call wall_slip(ux, uy, 3, .true.)

            !call bc_fixed_value_ew(uy, [(0e0, k=1,n-2)])
            !call bc_fixed_value_ww(uy, [(0e0, k=1,n-2)])

        end subroutine bc_u

        subroutine bc_p(p)
            real, intent(inout) :: p(:, :)

            integer :: m, n

            m = size( p, 1 )
            n = size( p, 2 )

            call bc_zero_gradient_sw(p)
            call bc_zero_gradient_ew(p)
            call bc_zero_gradient_nw(p)
            call bc_zero_gradient_ww(p)

            call bc_zero_gradient_bl(p)
            call bc_zero_gradient_br(p)
            call bc_zero_gradient_tl(p)
            call bc_zero_gradient_tr(p)
        end subroutine bc_p

        subroutine bc_d(d)
            real, intent(inout), target ::d(0:2, 1:NV*NH)

            integer :: i
            real, pointer :: d_grid(:, :)

            ! SW: R
            ! EW: G
            ! NW: R
            ! WW: G

            d_grid(1:NH, 1:NV) => d(0, 1:NH*NV)
            call bc_fixed_value_sw(d_grid, [(1e0, i=2, NV-1)])
            call bc_fixed_value_ew(d_grid, [(0e0, i=2, NH-1)])
            call bc_fixed_value_nw(d_grid, [(1e0, i=2, NH-1)])
            call bc_fixed_value_ww(d_grid, [(0e0, i=2, NV-1)])
            call bc_fixed_value_bl(d_grid, 0e0)
            call bc_fixed_value_br(d_grid, 0e0)
            call bc_fixed_value_tr(d_grid, 0e0)
            call bc_fixed_value_tl(d_grid, 0e0)

            d_grid(1:NH, 1:NV) => d(1, 1:NH*NV)
            call bc_fixed_value_sw(d_grid, [(0e0, i=2, NV-1)])
            call bc_fixed_value_ew(d_grid, [(1e0, i=2, NH-1)])
            call bc_fixed_value_nw(d_grid, [(0e0, i=2, NH-1)])
            call bc_fixed_value_ww(d_grid, [(1e0, i=2, NV-1)])
            call bc_fixed_value_bl(d_grid, 0e0)
            call bc_fixed_value_br(d_grid, 0e0)
            call bc_fixed_value_tr(d_grid, 0e0)
            call bc_fixed_value_tl(d_grid, 0e0)

            d_grid(1:NH, 1:NV) => d(2, 1:NH*NV)
            call bc_fixed_value_sw(d_grid, [(0e0, i=2, NV-1)])
            call bc_fixed_value_ew(d_grid, [(0e0, i=2, NH-1)])
            call bc_fixed_value_nw(d_grid, [(0e0, i=2, NH-1)])
            call bc_fixed_value_ww(d_grid, [(0e0, i=2, NV-1)])
            call bc_fixed_value_bl(d_grid, 0e0)
            call bc_fixed_value_br(d_grid, 0e0)
            call bc_fixed_value_tr(d_grid, 0e0)
            call bc_fixed_value_tl(d_grid, 0e0)
        end subroutine bc_d

        subroutine bc_d_r(d)
            real, intent(inout) :: d(:,:)

            integer :: m, n, k

            m = size( d , 1 )
            n = size( d , 2 )

            call bc_fixed_value_sw(d, [(5e-1 + 0e-1*cos(2e0*PI*t/T_P), k=2, m-1)])
            call bc_fixed_value_ew(d, [(5e-1 + 0e-1*sin(2e0*PI*t/T_P), k=2, n-1)])
            call bc_fixed_value_nw(d, [(5e-1 + 0e-1*cos(2e0*PI*t/T_P), k=2, m-1)])
            call bc_fixed_value_ww(d, [(5e-1 + 0e-1*sin(2e0*PI*t/T_P), k=2, n-1)])
            call bc_fixed_value_bl(d, 1e0)
            call bc_fixed_value_br(d, 0e0)
            call bc_fixed_value_tr(d, 1e0)
            call bc_fixed_value_tl(d, 0e0)

        end subroutine bc_d_r

        subroutine bc_d_g(d)
            real, intent(inout) :: d(:,:)

            integer :: m, n, k

            m = size( d , 1 )
            n = size( d , 2 )

            if(t <= 5e0 * DT) then
                call bc_fixed_value_sw(d, [(2e-1, k=2, m-1)])
                call bc_fixed_value_ew(d, [(2e-1, k=2, n-1)])
                call bc_fixed_value_nw(d, [(2e-1, k=2, m-1)])
                call bc_fixed_value_ww(d, [(2e-1, k=2, n-1)])
                call bc_fixed_value_bl(d, 0e0)
                call bc_fixed_value_br(d, 1e0)
                call bc_fixed_value_tr(d, 0e0)
                call bc_fixed_value_tl(d, 1e0)
            else
                call bc_fixed_value(d, [(0e0, k=1, 2*(m-1)+2*(n-1))])
            end if
        end subroutine bc_d_g

        subroutine bc_d_b(d)
            real, intent(inout) :: d(:,:)

            integer :: m, n, k

            m = size( d , 1 )
            n = size( d , 2 )

            call bc_fixed_value_sw(d, [(5e-1 + 0e-1*sin(2e0*PI*t/T_P), k=2, m-1)])
            call bc_fixed_value_ew(d, [(5e-1 + 0e-1*cos(2e0*PI*t/T_P), k=2, n-1)])
            call bc_fixed_value_nw(d, [(5e-1 + 0e-1*sin(2e0*PI*t/T_P), k=2, m-1)])
            call bc_fixed_value_ww(d, [(5e-1 + 0e-1*cos(2e0*PI*t/T_P), k=2, n-1)])
            call bc_fixed_value_bl(d, 0e0)
            call bc_fixed_value_br(d, 0e0)
            call bc_fixed_value_tr(d, 0e0)
            call bc_fixed_value_tl(d, 0e0)
        end subroutine bc_d_b

        subroutine add_f(ux, uy)
            real, intent(inout) :: ux(:,:)
            real, intent(inout) :: uy(:,:)

            call add_force_centripetal(ux, uy, DT, [LH, LV], RHO, OMEGA, [LH/2e0, LV/2e0])
        end subroutine add_f

    subroutine print_results(u)
        real, intent(in) :: u(:)

        integer :: i
        real :: ux(NV,NH), uy(NV,NH), p(NV,NH)

        ux = transpose(reshape(u(NM*0+1:NM*1), [NH, NV]))
        uy = transpose(reshape(u(NM*1+1:NM*2), [NH, NV]))
        p = transpose(reshape(u(NM*2+1:NM*3), [NH, NV]))

        write(*,*) "UX:"
        do i = 1, NV
            write(*,*) ux(i,:)
        end do

        write(*,*) "UY:"
        do i = 1, NV
            write(*,*) uy(i,:)
        end do

        write(*,*) "P:"
        do i = 1, NV
            write(*,*) p(i,:)
        end do

    end subroutine print_results


end program cfd_fortran_2